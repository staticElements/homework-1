#Assignment 1
#Team-4
#This python generator finds all the subsets of a set
#without using yeild


def subset (Xs):
    result = []
    if not Xs: result.append(Xs)
    else:
        X = Xs[0] #head
        Ys = Xs[1:] #tail
        for Zs in subset(Ys):
            result.append(Zs) #inherit from the subsets of tail
            result.append([X] + Zs) #extend from the subsets of tail

    return result

def go():
    for t in subset([0,1,2,3,4,5]): print(t)

go()
